ARG PYTORCH_VERSION="1.10.2"
# Select a base image from which to extend
FROM gitlab-registry.cern.ch/ai-ml/kubeflow_images/pytorch-notebook-gpu-1.8.1:v0.6.1-30

# or: FROM custom_public_registry/username/image
ARG FRAMEWORK="pytorch"
ARG PYTHON_VERSION="3.6.13"

USER root 

# Install required packages
COPY requirements.txt /requirements.txt
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys FEEA9169307EA071 8B57C5C2836F4BEB && apt-get -qq update && pip3 install -r /requirements.txt
RUN pip install mmcv-full -f https://download.openmmlab.com/mmcv/dist/10.2/1.10.2/index.html
RUN pip install mmsegmentation

USER jovyan




# The following line is mandatory:
CMD ["sh", "-c", \
     "jupyter lab --notebook-dir=/home/jovyan --ip=0.0.0.0 --no-browser \
      --allow-root --port=8888 --LabApp.token='' --LabApp.password='' \
      --LabApp.allow_origin='*' --LabApp.base_url=${NB_PREFIX}"]
